package main

//protoc --go_out=plugins=grpc:. *.proto

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	pb "ktproto/rpc实践/proto"
	"log"
)

const PORT = "9001"

func GetRpcData(client pb.SwpServiceClient) (*pb.SwpResponse, error) {

	resp, err := client.Swp(context.Background(), &pb.SwpRequest{
		Name: "swp",
	})

	return resp, err
}

func main() {
	conn, err := grpc.Dial(":"+PORT, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("grpc.Dial err :%v", err)
	}

	defer conn.Close()
	client := pb.NewSwpServiceClient(conn)
	data, err := GetRpcData(client)
	fmt.Println("或的的data的只未:", data)
	if err != nil {
		log.Fatalf("client.Search err:%v", err)
	}

	log.Printf("resp:%s", data)
}
