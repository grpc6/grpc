package main

import (
	"context"
	"google.golang.org/grpc"
	pb "ktproto/rpc实践/proto"
	"log"
	"net"
)

type SearchService struct{}

func (s *SearchService) Search(ctx context.Context, r *pb.SearchRequest) (*pb.SearchResponse, error) {
	return &pb.SearchResponse{
		Response: r.GetRequest() + "Server",
	}, nil
}

func (s *SearchService) Swp(ctx context.Context, r *pb.SwpRequest) (*pb.SwpResponse, error) {
	if r.Name == "swp" {
		return &pb.SwpResponse{
			Age:    "18",
			Height: "171",
			Sex:    "男",
		}, nil
	} else {
		return nil, nil
	}
}

const PORT = "9001"

func main() {
	server := grpc.NewServer()
	pb.RegisterSwpServiceServer(server, &SearchService{})

	lis, err := net.Listen("tcp", ":"+PORT)
	if err != nil {
		log.Fatalf("net.Listen err:%v", err)
	}

	server.Serve(lis)
}
